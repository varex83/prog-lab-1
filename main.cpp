#include <bits/stdc++.h>
#define double long double
using namespace std;
using namespace std::chrono;

const double MAX_RE_PRICE       = 1e6;
const double MAX_AREA           = 1e3;
const double MAX_LIVING_ROOMS   = 10;
const double MAX_SUBWAY_DIST    = 10;
const double MAX_BATH_ROOMS     = 10;

const int MAX_POPULATION_COUNT  = 100;
const double MUTATION_RATE      = 1e-2/2; // 0.5%
const int MOD                   = 1e9 + 7;
const int ITER_COUNT            = 1000 + 4; //

mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());

double rnd() {
    double r = (rng() % MOD);
    double rr = MOD;
    return r / rr; // rrr
}
 
class RealEstate {
public:
    double livingRooms;
    double bathRooms;
    double area;
    double subwayDist;
    double cost;
    // if type == true it's commercial - false if not
    bool type; 
    
    RealEstate(
        double livingRooms, 
        double bathRooms, 
        double area, 
        double subwayDist, 
        double cost,
        bool type
    ) : livingRooms(livingRooms / MAX_LIVING_ROOMS),
        bathRooms(bathRooms / MAX_BATH_ROOMS),
        area(area / MAX_AREA),
        subwayDist(subwayDist / MAX_SUBWAY_DIST),
        cost(cost / MAX_RE_PRICE),
        type(type) {}

    void normalize() {
        
    }

    vector <double> to_vector() {
        return {
            livingRooms,
            bathRooms,
            area,
            subwayDist,
            type,
            cost
        };
    }
};

class Genom {
public:
    vector <double> c;

public:
    
    Genom(int sz) {
        for (int i = 0; i < sz + 1; i++) {
            c.push_back(2 * rnd() - 1);
        }
    }

    Genom merge(Genom instance) {
        // cout << instance.c.size() << '\n';

        Genom new_ins((int)c.size() - 1);

        for (int i = 0; i < c.size(); i++) {
            if (rnd() < MUTATION_RATE) {
                continue;
            }
            new_ins.c[i] = (instance.c[i] + c[i]) / 2;
        }

        return new_ins;
    }

    // returns cost
    double fitness(vector <vector <double> > ts) {
        double ans = 0;
        
        for (int i = 0; i < ts.size(); i++) {
            double cur = calc(ts[i]);
            double tt = ts.size();
            ans += (cur - ts[i].back()) * (cur - ts[i].back()) / tt;
        }

        return ans;
    }

    double calc(vector <double> ts) {
        double cur = 0;
        for (int j = 0; j < c.size() - 1; j++) {
            cur += (c[j] * ts[j]);
        }
        cur += c.back();
        return cur;
    }
};

class Trainer {
public:
    vector <Genom> gens;
    vector <vector <double> > ts; // train sets
    int gen_size;
    Trainer(int gen_size) : gen_size(gen_size) {}
    
    void add(RealEstate instance) {
        ts.push_back(instance.to_vector());
    }

    // just one train iteration returns total fitness
    void iteration() {
        auto t = this->ts;

        int len = gens.size();
        for (int i = 0; i < len; i++) {
            for (int j = i + 1; j < len; j++) {
                auto child = gens[i].merge(gens[j]);
                gens.push_back(child);
            }       
        }      

        sort(gens.begin(), gens.end(), [t](Genom a, Genom b) {
            return a.fitness(t) < b.fitness(t);
        });

        while (gens.size() > MAX_POPULATION_COUNT) gens.pop_back();
    }   

    void train() {
        for (int i = 0; i < MAX_POPULATION_COUNT; i++) {
            gens.push_back(Genom(5));
        }

        for (int i = 0; i < ITER_COUNT; i++) {
            iteration();
            if (gens[0].fitness(ts) < (1e-15)) break;
        }
    }

    double get_result(vector <double> ts) {
        return gens[0].calc(ts) * MAX_RE_PRICE;
    }
};

void sanity_check(int livingRooms, int bathRooms,
        double area, double subwayDist, double cost,
        bool type) 
{
    if (livingRooms >= MAX_LIVING_ROOMS ||
        bathRooms   >= MAX_BATH_ROOMS   || 
        area        >= MAX_AREA         ||
        subwayDist  >= MAX_SUBWAY_DIST  ||
        cost        >= MAX_RE_PRICE    
    ) {
        cout << "Bad data provided. Aborting.\n";
        exit(0);
    }   
}

signed main () {
    int n;
    
    cout << "Input number of training sets\n";
    
    cin >> n;

    cout << "Input training sets in order: \n";
    cout << "(num of living rooms, num of bathRooms, area, subwayDist, cost, type(1/0))\n";

    Trainer trainer = Trainer(5);

    for (int i = 0; i < n; i++) {
        int livingRooms, bathRooms;
        double area, subwayDist, cost;
        bool type;

        cin >> livingRooms >> bathRooms;
        cin >> area >> subwayDist >> cost;
        cin >> type;

        sanity_check(
            livingRooms, bathRooms, area,
            subwayDist, cost, type
        );

        trainer.add(RealEstate(
            livingRooms,
            bathRooms, 
            area,
            subwayDist,
            cost,
            type
        ));

    }

    cout << "Training started.\n";
    
    auto t1 = high_resolution_clock::now();
   
    trainer.train();
   
    auto t2 = high_resolution_clock::now();
    duration<double, std::milli> ms_double = t2 - t1;
    
    cout << "Taining finished. Time taken: " << ms_double.count() << "ms\n";

    cout << "\n--------------------\n\n";
    cout << "To test trained system input number of test cases you want to test\n";

    cin >> n;

    cout << "Input test sets in order: \n";
    cout << "(num of living rooms, num of bathRooms, area, subwayDist, type(1/0))\n";

        for (int i = 0; i < n; i++) {
        int livingRooms, bathRooms;
        double area, subwayDist, cost;
        bool type;

        cin >> livingRooms >> bathRooms;
        cin >> area >> subwayDist;
        cin >> type;

        sanity_check(
            livingRooms, bathRooms, area,
            subwayDist, 0, type
        );

        cout << "Result is: ";

        cout << trainer.get_result(
            RealEstate(
                livingRooms,
                bathRooms, 
                area,
                subwayDist,
                0,
                type
            ).to_vector()
        ) << '\n';
    }

    return 0;
}